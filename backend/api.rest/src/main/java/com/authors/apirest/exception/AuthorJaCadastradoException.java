package com.authors.apirest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class AuthorJaCadastradoException extends  Exception{

    /**
	 * Excetion personalizada para autor ja cadastrado
	 */
	private static final long serialVersionUID = 1L;

	public AuthorJaCadastradoException() {
        super("autor já cadastrado");
    }
}
