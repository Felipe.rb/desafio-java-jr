package com.authors.apirest.author;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.authors.apirest.author.Author;

@Service
public interface AuthorRepository extends JpaRepository<Author, Long> {

	Author findById(long id);
	
}