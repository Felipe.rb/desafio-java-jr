package com.authors.apirest.author;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.authors.apirest.author.AuthorRepository;
import com.authors.apirest.exception.AuthorJaCadastradoException;
import com.authors.apirest.exception.AuthorNaoEncontradoException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Classe para mapear end-points author
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/authors/")
@Api(value="API REST Autores")
public class AuthorResource {
	
	/**
     * Injeção do service AuthorRepository
     */
	@Autowired
	private AuthorRepository authorRepository;
	
	/**
     * retorna todos os autores
     * @return lista de autores
     */
	@ApiOperation(value="Retorna uma lista de todos os Autores")
	@GetMapping
    @ResponseStatus(value = HttpStatus.OK)
    public List<Author> getAllAuthors(){
		List<Author> lista = this.authorRepository.findAll();
        return lista;
    }
	
	/**
     * Retorna o respectivo autor pelo id
     * @param id
     * @return autor
     * @throws AuthorNaoEncontradoException
     */
	@ApiOperation(value="Retorna um unico autor")
	@GetMapping("{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public Author getAuthorById(@PathVariable(value="id") long id) throws AuthorNaoEncontradoException {
        return this.authorRepository.findById(id);
    }
	
	/**
     * Adiciona um novo autor
     * @param author
     * @return author
     */
	@ApiOperation(value="Salva um autor")
	@PostMapping
	@ResponseStatus(value = HttpStatus.CREATED)
	public Author saveAuthor(@RequestBody Author author) throws AuthorJaCadastradoException {
		return this.authorRepository.save(author);
	}
	
	/**
     * Atualiza um autor pelo id
     * @param id
     * @return updatedAuthor
     */
	@ApiOperation(value="Atualiza um autor")
	@PutMapping("{id}")
	@ResponseStatus(value = HttpStatus.ACCEPTED)
	public Author updateAuthor(@Valid @NotNull @PathVariable(value = "id") long id, @RequestBody Author authorDetails) {
		Author author = authorRepository.findById(id);
		author.setName(authorDetails.getName());
		author.setEmail(authorDetails.getEmail());
		
		Author updatedAuthor = authorRepository.save(author);
	    return updatedAuthor;
	}
	
	/**
     * Exclui um autor pelo id
     * @param id
     * @return
     */
	@ApiOperation(value="Deleta um autor")
	@DeleteMapping("{id}")
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<?> deleteAuthor(@PathVariable(value = "id") long id) {
	    Author author = authorRepository.findById(id);

	    authorRepository.delete(author);

	    return ResponseEntity.ok().build();
	}
}
