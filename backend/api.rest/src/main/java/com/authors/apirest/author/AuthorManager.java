package com.authors.apirest.author;

import java.util.ArrayList;
import java.util.List;

public class AuthorManager {
	
	private List<Author> authors;
	private AuthorRepository authorRepository;

    public AuthorManager(){
        authors = new ArrayList<Author>();
    }
    
    
    public void addAuthor(Author author){
        this.authors.add(author);
    }
    
    
    public void removeAuthor(Author author){
        this.authors.remove(author);
    }
    
    
    public Author getAuthorPeloCPF(String cpf){
        for(Author a : this.authors){
            if(a!= null && a.getCpf().equals(cpf)){
                return a;
            }
        }
        return null;
    }
    
    
    public void removeAuthorPeloCPF(String cpf){
        Author author = getAuthorPeloCPF(cpf);
        this.authors.remove(author);
    }
    
    
    public List<Author> getTodosAuthors(){
        return this.authors;
    }
    
    
//    public Boolean authorAlreadyExists(Author author) {
//    	List<Author> authores = authorRepository.findAll();
//		for (int cada = 0; cada < authores.size(); cada++) {
//			if (authores.get(cada).getCpf().contains(author.getCpf())) {
//				return true;
//			} else if (authores.get(cada).getEmail().contains(author.getEmail())) {
//				return true;
//			}
//		}
//		return false;
//    }
}