package com.authors.apirest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class ObraJaCadastradaException extends  Exception{

    /**
	 * Excetion personalizada para obra ja cadastrada
	 */
	private static final long serialVersionUID = 1L;

	public ObraJaCadastradaException() {
        super("obra já cadastrada");
    }
}
