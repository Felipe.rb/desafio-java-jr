package com.authors.apirest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NO_CONTENT)
public class ObraNaoEncontradaException extends Exception {

    /**
	 * Excetion personalizada para obra sem cadastro
	 */
	private static final long serialVersionUID = 1L;

	public ObraNaoEncontradaException() {
        super("Nenhuma obra encontrada");
    }
}