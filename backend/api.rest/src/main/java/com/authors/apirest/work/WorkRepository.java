package com.authors.apirest.work;

import org.springframework.data.jpa.repository.JpaRepository;

import com.authors.apirest.work.Work;

public interface WorkRepository extends JpaRepository<Work, Long>{

	Work findById(long id);
}
