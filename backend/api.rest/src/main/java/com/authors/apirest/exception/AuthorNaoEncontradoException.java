package com.authors.apirest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NO_CONTENT)
public class AuthorNaoEncontradoException extends Exception {

    /**
	 * Excetion personalizada para autor sem cadastro
	 */
	private static final long serialVersionUID = 1L;

	public AuthorNaoEncontradoException() {
        super("Nenhum autor encontrado");
    }
}