package com.authors.apirest.work;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.authors.apirest.exception.ObraJaCadastradaException;
import com.authors.apirest.exception.ObraNaoEncontradaException;
import com.authors.apirest.work.Work;
import com.authors.apirest.work.WorkRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Classe para mapear end-points works
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/works/")
@Api(value="API REST Obras")
public class WorkResource {
	
	/**
     * Injeção do service WorkRepository
     */
	@Autowired
	private WorkRepository workRepository;
	
	/**
     * Retorna todas as obras
     * @return lista de obras
     */
	@ApiOperation(value="Retorna uma lista de todas as obras")
	@GetMapping
    @ResponseStatus(value = HttpStatus.OK)
    public List<Work> getAllWorks(){
        return this.workRepository.findAll();
    }
	
	/**
     * Retorna a respectiva obra pelo id
     * @param id
     * @return obra
     * @throws ObraNaoEncontradaException
     */
	@ApiOperation(value="Retorna uma obra")
	@GetMapping("/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public Work getWorkById(@PathVariable(value="id") long id) throws ObraNaoEncontradaException {
        return this.workRepository.findById(id);
    }
	
	/**
     * Adiciona uma nova obra
     * @param obra
     * @return obra
     */
	@ApiOperation(value="Salva uma obra")
	@PostMapping
	@ResponseStatus(value = HttpStatus.CREATED)
	public Work saveWork(@RequestBody Work work) throws ObraJaCadastradaException {
		List<Work> workes = workRepository.findAll();
		for (int cada = 0; cada < workes.size(); cada++) {
			if (workes.get(cada).getPublished().equals(null)) {
				if(workes.get(cada).getExhibition().equals(null)){
					return work;
				}
			}
		}
		return this.workRepository.save(work);
	}

	/**
     * Atualiza uma obra pelo id
     * @param id
     * @return updatedWork
     */
	@ApiOperation(value="Atualiza uma obra")
	@PutMapping("{id}")
	@ResponseStatus(value = HttpStatus.OK)
	public Work updateWork(@PathVariable(value = "id") long id, @RequestBody Work workDetails) {
		Work work = workRepository.findById(id);
		work.setName(workDetails.getName());
		work.setDescription(workDetails.getDescription());
		
		Work updatedWork = workRepository.save(work);
	    return updatedWork;
	}
	
	/**
     * Exclui uma obra pelo id
     * @param id
     * @return
     */
	@ApiOperation(value="Deleta uma obra")
	@DeleteMapping("{id}")
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<?> deleteWork(@PathVariable(value = "id") long id) {
	    Work work = workRepository.findById(id);

	    workRepository.delete(work);

	    return ResponseEntity.ok().build();
	}
}