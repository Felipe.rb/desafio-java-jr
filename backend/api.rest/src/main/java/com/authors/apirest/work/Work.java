package com.authors.apirest.work;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="TB_WORK", uniqueConstraints={@UniqueConstraint(columnNames={"id"})})
public class Work implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(nullable=false)
	private long id;
	
	@Column(nullable=false)
	private String name;
	
	@Column(nullable=false, length=240)
	private String description;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	@Column(nullable = true)
	private Date published;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	@Column(nullable = true)
	private Date exhibition;

	
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getPublished() {
		return published;
	}

	public void setPublished(Date published) {
		this.published = published;
	}

	public Date getExhibition() {
		return exhibition;
	}

	public void setExhibition(Date exhibition) {
		this.exhibition = exhibition;
	}
	
}