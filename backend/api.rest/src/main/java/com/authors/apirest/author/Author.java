package com.authors.apirest.author;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="TB_AUTHOR", uniqueConstraints={@UniqueConstraint(columnNames={"id","cpf", "email"})})
public class Author implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public enum Gender{ 
	    MALE, FEMALE 
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(nullable=false)
	private long id;
	
	@Column(nullable=false)
	private String name;
	
	@Column
	@Enumerated(EnumType.STRING)
	private Gender sex;
	
	@Column
	private String email;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	@Column(nullable=false)
    private Date birthday;
	
	@Column(nullable=false)
	private String country;
	
	@Column
	private String cpf;

	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Gender getSex() {
		return sex;
	}

	public void setSex(Gender sex) {
		this.sex = sex;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
}
