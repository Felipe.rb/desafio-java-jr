interface NavAttributes {
  [propName: string]: any;
}
interface NavWrapper {
  attributes: NavAttributes;
  element: string;
}
interface NavBadge {
  text: string;
  variant: string;
}
interface NavLabel {
  class?: string;
  variant: string;
}

export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: NavBadge;
  title?: boolean;
  children?: NavData[];
  variant?: string;
  attributes?: NavAttributes;
  divider?: boolean;
  class?: string;
  label?: NavLabel;
  wrapper?: NavWrapper;
}

export const navItems: NavData[] = [
  // {
  //   name: 'Dashboard',
  //   url: '/dashboard',
  //   icon: 'icon-speedometer',
  // },
  {
    title: true,
    name: 'Components'
  },
  {
    name: 'Author',
    url: '/author',
    icon: 'icon-user',
    children: [
      {
        name: 'List Author',
        url: '/author/list',
        icon: 'icon-list'
      },
      {
        name: 'Create Author',
        url: '/author/create',
        icon: 'icon-user-follow'
      },
      {
        name: 'Edit Author',
        url: '/author/edit',
        icon: 'icon-user-follow'
      }
    ]
  },
  {
    name: 'Work',
    url: '/work',
    icon: 'icon-user',
    children: [
      {
        name: 'List Work',
        url: '/work/list',
        icon: 'icon-list'
      },
      {
        name: 'Create Work',
        url: '/work/create',
        icon: 'icon-user-follow'
      },
      {
        name: 'Edit Work',
        url: '/work/edit',
        icon: 'icon-user-follow'
      }
    ]
  }
];
