import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Author} from './views/author/author';
import {Work} from './views/work/work';


import {catchError, tap} from 'rxjs/operators';

const httpOptions  = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET, POST, PUT'
  })

};
const apiUrl = 'http://localhost:8080';


@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private http: HttpClient) { }

  getAuthors (): Observable<Author[]> {
    const url = `${apiUrl}/authors/`;
    return this.http.get<Author[]>(url, httpOptions)
      .pipe(
        tap(author => console.log('leu os autores')),
        catchError(this.handleError('getAuthors', []))
      );
  }

  getAuthorById(id: number): Observable<Author> {
    const url = `${apiUrl}/authors/${id}`;
    return this.http.get<Author>(url, httpOptions).pipe(
      tap(_ => console.log(`leu o Author id=${id}`)),
      catchError(this.handleError<Author>(`getAuthor id=${id}`))
    );
  }

  addAuthor (author): Observable<Author> {
    const url = `${apiUrl}/authors/`;
    return this.http.post<Author>(url, author, httpOptions).pipe(
      // tslint:disable-next-line:no-shadowed-variable
      tap((author: Author) => console.log(`adicionou o Author com w/ id=${author.id}`)),
      catchError(this.handleError<Author>('addAuthor'))
    );
  }

  updateAuthor(id, author): Observable<any> {
    const url = `${apiUrl}/authors/${id}`;
    return this.http.put(url, author, httpOptions).pipe(
      tap(_ => console.log(`atualiza o Author com id=${id}`)),
      catchError(this.handleError<any>('updateAuthor'))
    );
  }

  deleteAuthor (id): Observable<Author> {
    const url = `${apiUrl}/authors/${id}`;

    return this.http.delete<Author>(url, httpOptions).pipe(
      tap(_ => console.log(`remove o Author com id=${id}`)),
      catchError(this.handleError<Author>('deleteAuthor'))
    );
  }


  getWorks (): Observable<Work[]> {
    const url = `${apiUrl}/works/`;
    return this.http.get<Work[]>(url, httpOptions)
      .pipe(
        tap(author => console.log('leu os Work')),
        catchError(this.handleError('getWork', []))
      );
  }

  getWorkById(id: number): Observable<Work> {
    const url = `${apiUrl}/works/${id}`;
    return this.http.get<Work>(url, httpOptions).pipe(
      tap(_ => console.log(`leu o Author id=${id}`)),
      catchError(this.handleError<Work>(`getWork id=${id}`))
    );
  }

  addWork (work): Observable<Work> {
    const url = `${apiUrl}/works/`;
    return this.http.post<Work>(url, work, httpOptions).pipe(
      // tslint:disable-next-line:no-shadowed-variable
      tap((work: Work) => console.log(`adicionou o work com w/ id=${work.id}`)),
      catchError(this.handleError<Work>('addwork'))
    );
  }

  updateWork(id, work): Observable<any> {
    const url = `${apiUrl}/works/${id}`;
    return this.http.put(url, work, httpOptions).pipe(
      tap(_ => console.log(`atualiza o Work com id=${id}`)),
      catchError(this.handleError<any>('updateWork'))
    );
  }

  deleteWork (id): Observable<Work> {
    const url = `${apiUrl}/works/${id}`;

    return this.http.delete<Work>(url, httpOptions).pipe(
      tap(_ => console.log(`remove o Work com id=${id}`)),
      catchError(this.handleError<Work>('deleteWork'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error);

      return of(result as T);
    };
  }
}
