export class Work {
  id: bigint;
  name: string;
  description: string;
  published: Date;
  exhibition: Date;
}
