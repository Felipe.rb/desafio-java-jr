import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WorkComponent } from './work.component';
import { CreateWorkComponent } from './create-work.component';
import {EditWorkComponent} from './edit-work.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Work'
    },
    children: [
      {
        path: '',
        redirectTo: 'work'
      },
      {
        path: 'list',
        component: WorkComponent,
        data: {
          title: 'List Work'
        }
      },
      {
        path: 'create',
        component: CreateWorkComponent,
        data: {
          title: 'Create Work'
        }
      }
      ,
      {
        path: 'edit',
        component: EditWorkComponent,
        data: {
          title: 'Create Work'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkRoutingModule {}
