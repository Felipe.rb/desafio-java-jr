import {Component, OnInit} from '@angular/core';

import { ApiService } from '../../api.service';
import { Work } from './work';

@Component({
  templateUrl: 'work.component.html'
})
export class WorkComponent implements OnInit {

  dataSource: Work[];

  constructor(private _api: ApiService) { }

  ngOnInit() {
    this._api.getWorks()
      .subscribe(res => {
        this.dataSource = res;
        console.log(this.dataSource);
      }, err => {
        console.log(err);
      });
  }

  deleteWork(id: bigint) {
    this._api.deleteWork(id)
      .subscribe(res => {
        console.log(this.dataSource);
      }, err => {
        console.log(err);
      });
  }
}
