import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


import { WorkComponent } from './work.component';
import { CreateWorkComponent } from './create-work.component';


// Buttons Routing
import { WorkRoutingModule } from './work-routing.module';
import {EditWorkComponent} from './edit-work.component';

// Angular

@NgModule({
  imports: [
    CommonModule,
    WorkRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    WorkComponent,
    CreateWorkComponent,
    EditWorkComponent
  ]
})
export class WorkModule { }
