import {Component, OnInit } from '@angular/core';

import { Work } from './work';
import { ApiService } from '../../api.service';

import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import {Author} from '../author/author';
@Component({
  templateUrl: 'create-work.component.html'
})
export class CreateWorkComponent implements OnInit {
  dataSource: Work[];

  work: Work;


  workForm: FormGroup;


  constructor(private formBuilder: FormBuilder, private _api: ApiService) { }

  ngOnInit() {

    this.workForm = this.formBuilder.group({
      'name' : [null, Validators.required],
      'description' : [null, Validators.required],
      'published' : [null, Validators.required],
      'exhibition' : [null, Validators.required]
    });
  }

  addWork(form: NgForm) {
    this._api.addWork(form)
      .subscribe(res => {
        const id = res['_id'];
      }, (err) => {
        console.log(err);
      });
  }
}
