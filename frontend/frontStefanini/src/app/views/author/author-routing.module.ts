import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthorComponent } from './author.component';
import { CreateAuthorComponent } from './create-author.component';
import {EditAuthorComponent} from './edit-author.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Author'
    },
    children: [
      {
        path: '',
        redirectTo: 'author'
      },
      {
        path: 'list',
        component: AuthorComponent,
        data: {
          title: 'List Author'
        }
      },
      {
        path: 'create',
        component: CreateAuthorComponent,
        data: {
          title: 'Create Author'
        }
      }
      ,
      {
        path: 'edit',
        component: EditAuthorComponent,
        data: {
          title: 'Create Author'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthorRoutingModule {}
