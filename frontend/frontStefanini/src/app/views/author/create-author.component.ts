import {Component, OnInit } from '@angular/core';

import { Author } from './author';
import { ApiService } from '../../api.service';

import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';


@Component({
  templateUrl: 'create-author.component.html'
})
export class CreateAuthorComponent implements OnInit {
  dataSource: Author[];

  author: Author;
  cpf: Boolean = false;


  authorForm: FormGroup;


  constructor(private formBuilder: FormBuilder, private _api: ApiService) { }

  ngOnInit() {

    this.authorForm = this.formBuilder.group({
      'name' : [null, Validators.required],
      'sex' : [null, [Validators.required, Validators.minLength(4)]],
      'email' : [null, Validators.required],
      'birthday' : [null, Validators.required],
      'country' : [null, Validators.required],
      'cpf' : [null],
    });


    // this._api.addAuthor(this.author)
    //   .subscribe(res => {
    //     console.log(res);
    //   }, err => {
    //     console.log(err);
    //   });
  }

  addAuthor(form: NgForm) {
    this._api.addAuthor(form)
      .subscribe(res => {
        const id = res['_id'];
      }, (err) => {
        console.log(form);

        console.log(err);
      });
  }



  verifyCountry(value: String) {
    if ("br" == value) {
      this.cpf = true
    }
    else {
      this.cpf = false
    }
    console.log(this.cpf);
  }
}
