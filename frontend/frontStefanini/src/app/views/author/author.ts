export class Author {
  id: bigint;
  name: string;
  sex: string;
  email: string;
  birthday: Date;
  country: string;
  cpf: string;
}
