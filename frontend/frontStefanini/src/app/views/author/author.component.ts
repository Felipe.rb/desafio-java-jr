import { Component, OnInit } from '@angular/core';

import { ApiService } from '../../api.service';
import { Author } from './author';


@Component({
  templateUrl: 'author.component.html'
})
export class AuthorComponent implements OnInit {

  dataSource: Author[];

  constructor(private _api: ApiService) { }

  ngOnInit() {
      this._api.getAuthors()
      .subscribe(res => {
        this.dataSource = res;
        console.log(this.dataSource);
      }, err => {
        console.log(err);
      });
    }

    deleteAuthor(id: bigint) {
      this._api.deleteAuthor(id)
        .subscribe(res => {
          console.log(this.dataSource);
        }, err => {
          console.log(err);
        });
    }

}
