import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NgModule } from '@angular/core';

import { AuthorComponent } from './author.component';
import { CreateAuthorComponent } from './create-author.component';

import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

// Buttons Routing
import { AuthorRoutingModule } from './author-routing.module';
import {EditAuthorComponent} from './edit-author.component';

// Angular

@NgModule({
  imports: [
    CommonModule,
    AuthorRoutingModule,
    FormsModule,
    ReactiveFormsModule

  ],
  declarations: [
    AuthorComponent,
    CreateAuthorComponent,
    EditAuthorComponent
  ]
})
export class AuthorModule { }
